.. prepare-operating-system:

#################
Installing python
#################

In order to support a wide range of operating systems (Linux, Windows, Mac), the
installation is broken up into two portions:

1. Prepare your operating system - Installing python and required python modules
2. :doc:`Downaloading and running the gateway software <install-gateway>`

.. toctree::
   :maxdepth: 1

   ../prepare-operating-system/debian
   ../prepare-operating-system/raspberry-pi
   ../prepare-operating-system/windows
