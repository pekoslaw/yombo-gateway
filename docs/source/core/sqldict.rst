.. index:: sqldict

.. _sqldict:

.. currentmodule:: yombo.core.sqldict

====================================
SQLDict (yombo.core.sqldict)
====================================
.. automodule:: yombo.core.sqldict

SQLDict Class
============================
.. autoclass:: SQLDict
   :members:

   .. automethod:: __init__

