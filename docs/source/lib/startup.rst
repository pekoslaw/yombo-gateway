.. index:: startup

.. _startup:

.. currentmodule:: yombo.lib.startup

===========================
Startup (yombo.lib.startup)
===========================
.. automodule:: yombo.lib.startup

Startup Class
==============
.. autoclass:: Startup
   :members:

   .. automethod:: __init__
