.. index:: devices

.. _devices:

.. currentmodule:: yombo.lib.devices

============================
Devices (yombo.lib.devices)
============================
.. automodule:: yombo.lib.devices

Devices Class
=================
.. autoclass:: Devices
   :members:

   .. automethod:: __init__

Device Class
==================
.. autoclass:: Device
   :members:

   .. automethod:: __init__
